import os
from PIL import Image
from argparse import ArgumentParser


def valid_image(parser, arg):
    if not os.path.isfile(arg):
        parser.error("File does not exist: {f}".format(f=arg))

    img = Image.open(arg)
    img.thumbnail((600, 600), Image.BICUBIC)
    return img


def valid_dir(parser, arg):
    if not os.path.isdir(arg):
        parser.error("Directory does not exist: {f}".format(f=arg))
    return str(arg)


parser = ArgumentParser()
parser.add_argument(
    "image", help="Full path to the input image.", type=lambda x: valid_image(parser, x)
)
parser.add_argument(
    "output",
    help="Full path to the output directory.",
    type=lambda x: valid_dir(parser, x),
)
parser.add_argument(
    "-b",
    "--boxes",
    required=False,
    help="If specified, also outputs an image of the inference result with all bounding boxes in the output directory. Useful for visually inspecting which components are actually detected by the model.",
    action="store_true",
)
parser.add_argument(
    "-a",
    "--accuracy",
    required=False,
    help="The minimum accepted accuracy for inference. Defaults to 0.5.",
    type=float,
    default=0.5,
)
parser.add_argument(
    "-p",
    "--pages",
    required=False,
    help="A non-zero positive integer that specifies how many pages are in the input image. Supplying a valid argument to this parameter forces the application to use KMeans clustering instead of the default DBSCAN algorithm.",
    type=int,
    default=0,
)

args = parser.parse_args()
