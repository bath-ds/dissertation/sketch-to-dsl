import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import object_detection
from inference import predict
from clustering import clustering
from pug import build_pug_content
from components.builder import build_component
from args import args


# convert image to numpy array
image_np = np.array(args.image)

# perform inference
output_dict = predict(image_np, args.accuracy, args.boxes)

# perform clustering
clusters = clustering(output_dict["mids"], pages=args.pages)

# separate by page
pages = []
for i in set(clusters):
    if i == -1:
        continue
    d = dict()
    d["num_detections"] = np.sum(clusters == i)
    d["boxes"] = output_dict["boxes"][clusters == i]
    d["detection_boxes"] = output_dict["detection_boxes"][clusters == i]
    d["detection_scores"] = output_dict["detection_scores"][clusters == i]
    d["detection_classes"] = output_dict["detection_classes"][clusters == i]
    d["detection_labels"] = output_dict["detection_labels"][clusters == i]
    d["mids"] = output_dict["mids"][clusters == i]
    # set page id based on page id component
    d["id"] = i  # default to number
    for i in d["detection_labels"]:
        if i.startswith("page"):
            d["id"] = i[-1]
            break
    # check for nav components, if exists check component colors on that page
    nav_exists = np.any([c.startswith("nav") for c in d["detection_labels"]])
    components = []
    for j in range(d["num_detections"]):
        box = np.array(d["boxes"][j], dtype="int32")
        c = build_component(
            class_id=d["detection_classes"][j],
            label=d["detection_labels"][j],
            box=d["boxes"][j],
            img=image_np[box[1] : box[3], box[0] : box[2], :],  # height, width
            mid=d["mids"][j],
            score=d["detection_scores"][j],
            extract_color=nav_exists,
        )
        components.append(c)

    d["components"] = np.array([x for x in components if x.is_render])
    d["meta"] = [x for x in components if not x.is_render]

    # determine nav to
    for n in (x for x in d["meta"] if x.label.startswith("nav")):
        for c in d["components"]:
            if c.color is not None and c.color == n.color:
                c.link_to = n.label[-1]

    # group similar y values
    r = 10
    for c in components:
        if c.avg_mid is None:
            for x in components:
                if x.avg_mid is None and x.mid[1] - r <= c.mid[1] < x.mid[1] + r:
                    x.avg_mid = np.array([x.mid[0], c.mid[1]])

    pages.append(d)

# sort components by xy
for page in pages:
    temp = np.array(
        [tuple(i.avg_mid) for i in page["components"]],
        dtype=[("x", "int32"), ("y", "int32")],
    )
    temp = np.argsort(temp, order=("y", "x"))
    page["components"] = page["components"][temp]

    # build pug and actions file
    filename = "page"
    pug_filename = os.path.join(
        args.output, "{f}_{i}.pug".format(f=filename, i=page["id"])
    )
    lines = build_pug_content(page["components"])

    # write file
    with open(pug_filename, "w") as writer:
        for line in lines:
            if line:
                writer.write(line)

# output bounding boxes
if args.boxes:
    bb_path = os.path.join(args.output, "inference_result.jpg")
    plt.imsave(bb_path, output_dict["bb_image"])

