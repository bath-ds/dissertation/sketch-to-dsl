import os
import numpy as np
import tensorflow as tf
from collections import defaultdict
from object_detection.utils import visualization_utils as vis_util
from object_detection.utils import label_map_util

dirname = os.path.dirname(__file__)

model_path = os.path.join(
    dirname, "model", "output_inference_graph_v1.pb", "frozen_inference_graph.pb"
)
detection_graph = tf.Graph()

label_path = os.path.join(dirname, "model", "label_map.pbtxt")
label_map = label_map_util.create_category_index_from_labelmap(
    label_path, use_display_name=False
)
category_index = label_map_util.create_category_index_from_labelmap(label_path, use_display_name=True)

with detection_graph.as_default():
    od_graph_def = tf.compat.v1.GraphDef()
    with tf.io.gfile.GFile(model_path, "rb") as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name="")


def predict(image_ori, min_pct=0, boxes=True):
    # The model expects images to have shape: [1, None, None, 3]
    image = np.expand_dims(image_ori, axis=0)
    with detection_graph.as_default():
        with tf.compat.v1.Session() as sess:
            ops = tf.compat.v1.get_default_graph().get_operations()
            tensors = {output.name for op in ops for output in op.outputs}
            tensor_dict = {}
            for key in [
                "num_detections",
                "detection_boxes",
                "detection_scores",
                "detection_classes",
            ]:
                name = key + ":0"
                if name in tensors:
                    tensor_dict[
                        key
                    ] = tf.compat.v1.get_default_graph().get_tensor_by_name(name)
            image_tensor = tf.compat.v1.get_default_graph().get_tensor_by_name(
                "image_tensor:0"
            )

            # Run inference
            result = sess.run(tensor_dict, feed_dict={image_tensor: image})

            result["num_detections"] = int(result["num_detections"][0])
            result["detection_classes"] = result["detection_classes"][0].astype(
                np.int64
            )
            result["detection_boxes"] = result["detection_boxes"][0]
            result["detection_scores"] = result["detection_scores"][0]

    # parameterize min percentage to filter out
    if min_pct > 0:
        idx = result["detection_scores"] > min_pct
        result["num_detections"] = np.sum(idx)
        result["detection_boxes"] = result["detection_boxes"][idx]
        result["detection_scores"] = result["detection_scores"][idx]
        result["detection_classes"] = result["detection_classes"][idx]
        result["detection_labels"] = np.array(
            [label_map[i]["name"] for i in result["detection_classes"]]
        )

    # revert to original dimensions and re-arrange xy
    image_width = image.shape[2]
    image_height = image.shape[1]
    result["boxes"] = np.empty_like(result["detection_boxes"])
    result["boxes"][:, 0] = result["detection_boxes"][:, 1] * image_width  # xmin
    result["boxes"][:, 1] = result["detection_boxes"][:, 0] * image_height  # ymin
    result["boxes"][:, 2] = (result["detection_boxes"][:, 3] * image_width) + 4  # xmax
    result["boxes"][:, 3] = (result["detection_boxes"][:, 2] * image_height) + 4  # ymax

    # build center points
    result["mids"] = np.array(
        [[np.mean([i[0], i[2]]), np.mean([i[1], i[3]])] for i in result["boxes"]]
    )

    # generate image with bounding boxes
    if boxes:
        image_copy = np.copy(image_ori)
        vis_util.visualize_boxes_and_labels_on_image_array(
            image_copy,
            result["detection_boxes"],
            result["detection_classes"],
            result["detection_scores"],
            category_index,
            use_normalized_coordinates=True,
            line_thickness=1,
            min_score_thresh=min_pct,
            max_boxes_to_draw=200,
        )
        result["bb_image"] = image_copy

    return result
