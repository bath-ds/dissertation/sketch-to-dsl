import numpy as np


def build_pug_content(components):  # sorted components
    # build pug content
    lines = ["Container\n"]
    level = 1
    last_y = None
    for c in components:
        if last_y != c.avg_mid[1]:
            level = 1
            # look for same rounded mid y
            if (
                np.sum(
                    np.array([cc.avg_mid[1] for cc in components])
                    == c.avg_mid[1]
                )
                > 1
            ):  # same_y = > 1:
                level += 1
                lines.append("\tRow\n")
        last_y = c.avg_mid[1]
        lines.append(c.compile_pug(level))

    return lines
