from components.component import Component
import numpy as np


class Image(Component):
    def __init__(self, *args, **kwargs):
        self.is_render = True
        return super().__init__(*args, **kwargs)

    def compile_pug(self, level=1):
        if self.link_to:
            return super()._pug_str_helper(
                'Image(source="https://placeimg.com/400/400/any?a={r}" link_to="{p}")'.format(
                    p=self.link_to, r=np.random.random_integers(1, 1000)
                ),
                level,
            )
        return super()._pug_str_helper(
            'Image(source="https://placeimg.com/400/400/any?a={r}")'.format(
                r=np.random.random_integers(1, 1000)
            ),
            level,
        )