import cv2
import numpy as np

# hsv definitions, lower upper
red = np.array([[0, 40, 40], [20, 255, 255]])
green = np.array([[40, 40, 40], [90, 255, 255]])
blue = np.array([[91, 40, 40], [130, 255, 255]])
colors = ["red", "green", "blue"]


class Component:
    def __init__(self, *args, **kwargs):
        self.class_id = kwargs["class_id"]
        self.label = kwargs["label"]
        self.box = kwargs["box"]
        self.img = kwargs["img"]
        self.mid = kwargs["mid"]
        self.score = kwargs["score"]
        self.link_to = kwargs.get("link_to", None)
        self.is_render = not kwargs["label"].startswith("nav") and not kwargs[
            "label"
        ].startswith("page")
        self.color = self.get_color() if kwargs.get("extract_color", False) else None
        self.avg_mid = None

    def get_color(self):
        # return 'red', 'green', 'blue', or None based on dominant color
        color = None
        hsv = cv2.cvtColor(self.img, cv2.COLOR_RGB2HSV)

        # get dominant color
        pcts = []
        for i in [red, green, blue]:
            mask = cv2.inRange(hsv, i[0], i[1])
            # calculate percentage
            total = mask.size
            count = np.sum(mask > 0)
            pcts.append(count / total)

        # check for color that passes 1% of total pixels
        if np.max(pcts) >= 0.01:
            color = colors[np.argmax(pcts)]

        return color

    def compile_pug(self, level=1):
        pass

    def _pug_str_helper(self, string, level=1):
        return '{t}{s}\n'.format(t='\t' * level, s=string)

