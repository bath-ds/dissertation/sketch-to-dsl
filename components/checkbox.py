from components.component import Component
from faker import Faker
import numpy as np


class CheckBox(Component):
    def __init__(self, *args, **kwargs):
        self.is_render = True
        self.text = " ".join(
            Faker().words(np.random.random_integers(1, 3))
        ).capitalize()
        return super().__init__(*args, **kwargs)

    def compile_pug(self, level=1):
        return super()._pug_str_helper("CheckBox {t}".format(t=self.text), level)

