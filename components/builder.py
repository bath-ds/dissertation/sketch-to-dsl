from components.component import Component

from components.button import Button
from components.title_bar import TitleBar
from components.textbox import TextBox
from components.image import Image
from components.text import Text
from components.checkbox import CheckBox
from components.radio_button import RadioButton
from components.dropdown import Dropdown
from components.heading import Heading
from components.paragraph import Paragraph


def build_component(**kwargs):
    if kwargs["label"] == "button":
        return Button(**kwargs)
    elif kwargs["label"] == "title-bar":
        return TitleBar(**kwargs)
    elif kwargs["label"] == "textbox":
        return TextBox(**kwargs)
    elif kwargs["label"] == "image":
        return Image(**kwargs)
    elif kwargs["label"] == "text":
        return Text(**kwargs)
    elif kwargs["label"] == "checkbox":
        return CheckBox(**kwargs)
    elif kwargs["label"] == "radio-button":
        return RadioButton(**kwargs)
    elif kwargs["label"] == "dropdown":
        return Dropdown(**kwargs)
    elif kwargs["label"] == "heading":
        return Heading(**kwargs)
    elif kwargs["label"] == "paragraph":
        return Paragraph(**kwargs)
    return Component(**kwargs)

