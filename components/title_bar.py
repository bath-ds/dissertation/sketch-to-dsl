from components.component import Component
from faker import Faker
import numpy as np


class TitleBar(Component):
    def __init__(self, *args, **kwargs):
        self.is_render = True
        self.text = " ".join(
            [i.capitalize() for i in Faker().words(np.random.random_integers(1, 2))]
        )
        return super().__init__(*args, **kwargs)

    def compile_pug(self, level=1):
        return super()._pug_str_helper(
            'TitleBar(title="{t}")'.format(t=self.text), level
        )