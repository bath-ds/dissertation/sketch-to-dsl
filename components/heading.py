from components.component import Component
from faker import Faker
import numpy as np


class Heading(Component):
    def __init__(self, *args, **kwargs):
        self.is_render = True
        self.text = " ".join(
            Faker().words(np.random.random_integers(1, 4))
        ).capitalize()
        return super().__init__(*args, **kwargs)

    def compile_pug(self, level=1):
        if self.link_to:
            return super()._pug_str_helper(
                'Heading(link_to="{p}") {t}'.format(p=self.link_to, t=self.text), level
            )
        return super()._pug_str_helper("Heading {t}".format(t=self.text), level)

