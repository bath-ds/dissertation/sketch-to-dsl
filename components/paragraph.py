from components.component import Component
from faker import Faker
import numpy as np


class Paragraph(Component):
    def __init__(self, *args, **kwargs):
        self.is_render = True
        self.text = Faker().paragraph(np.random.random_integers(4, 10))
        return super().__init__(*args, **kwargs)

    def compile_pug(self, level=1):
        if self.link_to:
            return super()._pug_str_helper(
                'Paragraph(link_to="{p}") {t}'.format(p=self.link_to, t=self.text), level
            )
        return super()._pug_str_helper("Paragraph {t}".format(t=self.text), level)

