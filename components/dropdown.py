from components.component import Component
from faker import Faker
import numpy as np


class Dropdown(Component):
    def __init__(self, *args, **kwargs):
        self.is_render = True
        self.text = " ".join(
            Faker().words(np.random.random_integers(1, 3))
        ).capitalize()
        return super().__init__(*args, **kwargs)

    def compile_pug(self, level=1):
        s = super()._pug_str_helper('Dropdown(placeholder="{t}")'.format(t=self.text), level)
        for _ in range(np.random.random_integers(2, 6)):
            s += super()._pug_str_helper(
                'DropdownItem(label="{t}")'.format(
                    t=" ".join(Faker().words(np.random.random_integers(1, 3))).capitalize()
                ),
                level + 1,
            )
        return s
