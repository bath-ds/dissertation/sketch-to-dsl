from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans


def clustering(coords, min_dist=80, min_samples=2, pages=0):
    if pages > 0:  # KMeans when page is specified
        return KMeans(n_clusters=pages).fit_predict(coords)
    return DBSCAN(eps=min_dist, min_samples=min_samples).fit_predict(coords)

